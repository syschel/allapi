from django.shortcuts import render
from django.views.generic.base import RedirectView, TemplateView


class Main(TemplateView):
    template_name = 'home.html'